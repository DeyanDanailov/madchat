const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const mongoose = require('mongoose');
let port = process.env.PORT || 5000;

app.use(express.static(__dirname))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

const dbUrl = 'mongodb+srv://admin:admin@madchatdb.6jcix.mongodb.net/madchatDatabase?retryWrites=true&w=majority&ssl=true';

const Message = mongoose.model('Message', {
    name: String,
    message: String
})

app.get('/messages', (req, res) => {
    Message.find({}, (err, messages) => {
        res.send(messages)
    });
})

app.post('/messages', (req, res) => {
    let message = new Message(req.body);
    message.save((err) => {
        if (err) {
            res.sendStatus(500)
            console.log(err.message)
        }

        io.emit('message', req.body)
        res.sendStatus(200)

    })
})
io.on('connection', (socket) => {
    console.log('user connected');
})

mongoose.connect(dbUrl, (err) => {
    if (err) console.log(err)
    else console.log('mongodb connection successful')
})

const server = http.listen(port, () => {
    console.log('listening on port... %d', port);
})
